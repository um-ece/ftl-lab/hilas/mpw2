// SPDX-FileCopyrightText: 2020 Efabless Corporation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// SPDX-License-Identifier: Apache-2.0

`default_nettype none
/*
 *-------------------------------------------------------------
 *
 * user_analog_project_wrapper
 *
 * This wrapper enumerates all of the pins available to the
 * user for the user analog project.
 *
 *-------------------------------------------------------------
 */

module user_analog_project_wrapper (
`ifdef USE_POWER_PINS
    inout vdda1,	// User area 1 3.3V supply
    inout vdda2,	// User area 2 3.3V supply
    inout vssa1,	// User area 1 analog ground
    inout vssa2,	// User area 2 analog ground
    inout vccd1,	// User area 1 1.8V supply
    inout vccd2,	// User area 2 1.8v supply
    inout vssd1,	// User area 1 digital ground
    inout vssd2,	// User area 2 digital ground
`endif

    // Wishbone Slave ports (WB MI A)
    input wb_clk_i,
    input wb_rst_i,
    input wbs_stb_i,
    input wbs_cyc_i,
    input wbs_we_i,
    input [3:0] wbs_sel_i,
    input [31:0] wbs_dat_i,
    input [31:0] wbs_adr_i,
    output wbs_ack_o,
    output [31:0] wbs_dat_o,

    // Logic Analyzer Signals
    input  [127:0] la_data_in,
    output [127:0] la_data_out,
    input  [127:0] la_oenb,

    /* GPIOs.  There are 27 GPIOs, on either side of the analog.
     * These have the following mapping to the GPIO padframe pins
     * and memory-mapped registers, since the numbering remains the
     * same as caravel but skips over the analog I/O:
     *
     * io_in/out/oeb/in_3v3 [26:14]  <--->  mprj_io[37:25]
     * io_in/out/oeb/in_3v3 [13:0]   <--->  mprj_io[13:0]
     *
     * When the GPIOs are configured by the Management SoC for
     * user use, they have three basic bidirectional controls:
     * in, out, and oeb (output enable, sense inverted).  For
     * analog projects, a 3.3V copy of the signal input is
     * available.  out and oeb must be 1.8V signals.
     */

    input  [`MPRJ_IO_PADS-`ANALOG_PADS-1:0] io_in,
    input  [`MPRJ_IO_PADS-`ANALOG_PADS-1:0] io_in_3v3,
    output [`MPRJ_IO_PADS-`ANALOG_PADS-1:0] io_out,
    output [`MPRJ_IO_PADS-`ANALOG_PADS-1:0] io_oeb,

    /* Analog (direct connection to GPIO pad---not for high voltage or
     * high frequency use).  The management SoC must turn off both
     * input and output buffers on these GPIOs to allow analog access.
     * These signals may drive a voltage up to the value of VDDIO
     * (3.3V typical, 5.5V maximum).
     * 
     * Note that analog I/O is not available on the 7 lowest-numbered
     * GPIO pads, and so the analog_io indexing is offset from the
     * GPIO indexing by 7, as follows:
     *
     * gpio_analog/noesd [17:7]  <--->  mprj_io[35:25]
     * gpio_analog/noesd [6:0]   <--->  mprj_io[13:7]
     *
     */
    
    inout [`MPRJ_IO_PADS-`ANALOG_PADS-10:0] gpio_analog,
    inout [`MPRJ_IO_PADS-`ANALOG_PADS-10:0] gpio_noesd,

    /* Analog signals, direct through to pad.  These have no ESD at all,
     * so ESD protection is the responsibility of the designer.
     *
     * user_analog[10:0]  <--->  mprj_io[24:14]
     *
     */
    inout [`ANALOG_PADS-1:0] io_analog,

    /* Additional power supply ESD clamps, one per analog pad.  The
     * high side should be connected to a 3.3-5.5V power supply.
     * The low side should be connected to ground.
     *
     * clamp_high[2:0]   <--->  mprj_io[20:18]
     * clamp_low[2:0]    <--->  mprj_io[20:18]
     *
     */
    inout [2:0] io_clamp_high,
    inout [2:0] io_clamp_low,

    // Independent clock (on independent integer divider)
    input   user_clock2,

    // User maskable interrupt signals
    output [2:0] user_irq
);

/*--------------------------------------*/
/* User project is instantiated  here   */
/*--------------------------------------*/
sky130_hilas_TopProtectStructure mprj (
    `ifdef USE_POWER_PINS
        //Power Connections
        .VDDA1(vdda1),
        .VSSA1(vssa1),
        .VCCD1(vccd1),
    `endif
    //IO Connections
    .IO7(io_in[7]),
    .IO8(io_in[8]),
    .IO9(io_in[9]),
    .IO10(io_in[10]),
    .IO11(io_in[11]),
    .IO12(io_in[12]),
    .IO13(io_in[13]),
    .IO25(io_in[25]),
    .IO26(io_in[26]),
    .IO27(io_in[27]),
    .IO28(io_in[28]),
    .IO29(io_in[29]),
    .IO30(io_in[30]),
    .IO31(io_in[31]),
    .IO32(io_in[32]),
    .IO33(io_in[33]),
    .IO34(io_in[34]),
    .IO35(io_in[35]),
    .IO36(io_in[36]),
    .IO37(io_in[37]),
    //Analog
    .ANALOG00(io_analog[0]),
    .ANALOG01(io_analog[1]),
    .ANALOG02(io_analog[2]),
    .ANALOG03(io_analog[3]),
    .ANALOG04(io_analog[4]),
    .ANALOG05(io_analog[5]),
    .ANALOG06(io_analog[6]),
    .ANALOG07(io_analog[7]),
    .ANALOG08(io_analog[8]),
    .ANALOG09(io_analog[9]),
    .ANALOG10(io_analog[10]),
    //Logic Analyzer OUT
    .LADATAOUT00(la_data_out[0]),
    .LADATAOUT01(la_data_out[1]),
    .LADATAOUT02(la_data_out[2]),
    .LADATAOUT03(la_data_out[3]),
    .LADATAOUT04(la_data_out[4]),
    .LADATAOUT05(la_data_out[5]),
    .LADATAOUT06(la_data_out[6]),
    .LADATAOUT07(la_data_out[7]),
    .LADATAOUT08(la_data_out[8]),
    .LADATAOUT09(la_data_out[9]),
    .LADATAOUT10(la_data_out[10]),
    .LADATAOUT11(la_data_out[11]),
    .LADATAOUT12(la_data_out[12]),
    .LADATAOUT13(la_data_out[13]),
    .LADATAOUT14(la_data_out[14]),
    .LADATAOUT15(la_data_out[15]),
    .LADATAOUT16(la_data_out[16]),
    .LADATAOUT17(la_data_out[17]),
    .LADATAOUT18(la_data_out[18]),
    .LADATAOUT19(la_data_out[19]),
    .LADATAOUT20(la_data_out[20]),
    .LADATAOUT21(la_data_out[21]),
    .LADATAOUT22(la_data_out[22]),
    .LADATAOUT23(la_data_out[23]),
    .LADATAOUT24(la_data_out[24]),
    //Logic Analyzer IN
    .LADATAIN00(la_data_in[0]),
    .LADATAIN01(la_data_in[1]),
    .LADATAIN02(la_data_in[2]),
    .LADATAIN03(la_data_in[3]),
);

endmodule	// user_analog_project_wrapper

`default_nettype wire
